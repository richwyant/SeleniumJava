# SeleniumJava<br>
17Jan2022<br>
This was originally hosted on my old GitHub portfolio and where I taught myself much about JUnit and Selenium Grid. Work was meant to continue in SeleniumJavaGradle, but life got in the way. Ported it over for archive purposes. This project is not maintained.<br>
<br>
12Dec2018<br>
As of today, work on this repo will be done on SeleniumJavaGradle. This will remain as an archive. 
<br>
Richard.
